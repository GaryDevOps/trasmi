import { Link } from 'react-router-dom';
import { NavContainer } from '../styled-components/Navbar.styled';


function Navbar() {

  return (
    <>
      <NavContainer>
        <img src="src/logo-transmilenio.png" alt='Logo'></img>
        <h2><span>TRANSMIQUEJAS</span></h2>
        <div>
          <Link className = "links" to="/">Home</Link>
          <Link to="/login">Login</Link>
          <Link to="/register">Register</Link>
          <Link to="/private/users">Usuarios</Link>
        </div>
      </NavContainer>
    </>
  )
}

export default Navbar;