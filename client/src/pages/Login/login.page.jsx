import { Formik } from 'formik';
import { useLoginContext } from '../../context/LoginProvider.Context'
import { StyleForm, Input } from '../../styled-components/register.styled';
import * as Yup from 'yup';
import {Div}from'../../styled-components/login.styled';

const SignupSchema = Yup.object().shape({
  email: Yup.string()
    .email('Correo electronico invalido')
});

function LoginPage() {
  const { createLogin } = useLoginContext();

  return (
    <div>
      <Div>
        <h3 className = "nuevo">TransmiLogueo</h3>
        <h3 className = "new">Ingresa con tus transmidatos</h3>
      </Div>
      
      <Formik
        initialValues={{
          email: "",
          password: "",
        }}
        validationSchema={SignupSchema}
        onSubmit={ async (values, actions) => {
          createLogin(values);
          actions.resetForm();
        }}
      >
        {({handleChange, handleSubmit, isSubmitting, values, errors, touched}) => (
          <StyleForm onSubmit={handleSubmit}>

            <Input type="text" placeholder="Correo electronico" name="email" onChange={handleChange} value={values.email}/> {
              errors.email && touched.email ? (
                <div>{errors.email}</div>
              ) : null
            } <br/> 

            <Input type="password" placeholder="Contraseña" name="password" onChange={handleChange} value={values.password}/> 

            <button type='submit' disabled={isSubmitting}>
              {isSubmitting ? "Guardando..." : "Entrar"}
            </button>
        
          </StyleForm>
        )}
      </Formik>
    </div>
  );
};

export default LoginPage;