import { Formik } from 'formik';
import { useLoginContext } from '../../context/LoginProvider.Context'
import { StyleForm, Input } from '../../styled-components/register.styled';
import * as Yup from 'yup';
import {Div}from'../../styled-components/login.styled';

function ClaimsPage() {
  return (
    <div class="fondo-claims">
      <div class="contenedor">
        <Div>
          <h3 className = "nuevo">TransmiQuejas</h3>
        </Div>
        <div class="caja-uno">
          <h3 className = "new">En esta sección irán las quejas de los usuarios</h3>
          <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Eius repellendus ex itaque aspernatur officia, quia porro, corrupti cumque nemo dignissimos quaerat magni ut eum velit sit, ratione inventore nesciunt ipsam!</p>
        </div>
        <div class="caja-dos">
          <h3 className = "new">En esta sección irán las quejas de los usuarios</h3>
          <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Eius repellendus ex itaque aspernatur officia, quia porro, corrupti cumque nemo dignissimos quaerat magni ut eum velit sit, ratione inventore nesciunt ipsam!</p>
        </div>
        <div class="caja-tres">
          <h3 className = "new">En esta sección irán las quejas de los usuarios</h3>
          <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Eius repellendus ex itaque aspernatur officia, quia porro, corrupti cumque nemo dignissimos quaerat magni ut eum velit sit, ratione inventore nesciunt ipsam!</p>
        </div>
        <div class="caja-cuatro">
          <h3 className = "new">En esta sección irán las quejas de los usuarios</h3>
          <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Eius repellendus ex itaque aspernatur officia, quia porro, corrupti cumque nemo dignissimos quaerat magni ut eum velit sit, ratione inventore nesciunt ipsam!</p>
        </div>
        <div class="caja-cinco">
          <h3 className = "new">En esta sección irán las quejas de los usuarios</h3>
          <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Eius repellendus ex itaque aspernatur officia, quia porro, corrupti cumque nemo dignissimos quaerat magni ut eum velit sit, ratione inventore nesciunt ipsam!</p>
        </div>
      </div>
    </div>
  );
  
}

export default ClaimsPage